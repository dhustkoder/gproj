#ifndef GPROJ_PLAYER_H_
#define GPROJ_PLAYER_H_
#include "types.h"

extern void player_init(void);
extern void player_update(uint32_t now, float dt);


#endif